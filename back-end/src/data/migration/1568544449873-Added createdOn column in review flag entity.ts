import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedCreatedOnColumnInReviewFlagEntity1568544449873 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `reviews_votes` DROP FOREIGN KEY `FK_b225ec6b6a5811efc56aaef4b31`");
        await queryRunner.query("ALTER TABLE `reviews_votes` CHANGE `reviewId` `reviewId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP FOREIGN KEY `FK_4d550ca9301c46d2dababd34438`");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP FOREIGN KEY `FK_93405d7e2e6308c3ba6b42f6c48`");
        await queryRunner.query("ALTER TABLE `reviews_flags` CHANGE `authorId` `authorId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `reviews_flags` CHANGE `reviewId` `reviewId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `books_reviews` DROP FOREIGN KEY `FK_b5b1376760b41852a8ffd40a915`");
        await queryRunner.query("ALTER TABLE `books_reviews` DROP FOREIGN KEY `FK_d990fae43ca0533c6d5d4668cdf`");
        await queryRunner.query("ALTER TABLE `books_reviews` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `books_reviews` CHANGE `authorId` `authorId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `books_reviews` CHANGE `bookId` `bookId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `books_rating` DROP FOREIGN KEY `FK_e183e272ec7c5cd48cd09768848`");
        await queryRunner.query("ALTER TABLE `books_rating` DROP FOREIGN KEY `FK_85051608ff543aabf9c083b2590`");
        await queryRunner.query("ALTER TABLE `books_rating` CHANGE `userId` `userId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `books_rating` CHANGE `bookId` `bookId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `books` DROP FOREIGN KEY `FK_bb8627d137a861e2d5dc8d1eb20`");
        await queryRunner.query("ALTER TABLE `books` CHANGE `userId` `userId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_73284a017a1b03deabe9ed279e1`");
        await queryRunner.query("ALTER TABLE `users` CHANGE `registeredOn` `registeredOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `users` CHANGE `banStatusId` `banStatusId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `ban_statuses` CHANGE `bannedOn` `bannedOn` datetime NULL DEFAULT null");
        await queryRunner.query("ALTER TABLE `reviews_votes` ADD CONSTRAINT `FK_b225ec6b6a5811efc56aaef4b31` FOREIGN KEY (`reviewId`) REFERENCES `books_reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD CONSTRAINT `FK_4d550ca9301c46d2dababd34438` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD CONSTRAINT `FK_93405d7e2e6308c3ba6b42f6c48` FOREIGN KEY (`reviewId`) REFERENCES `books_reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_reviews` ADD CONSTRAINT `FK_b5b1376760b41852a8ffd40a915` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_reviews` ADD CONSTRAINT `FK_d990fae43ca0533c6d5d4668cdf` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_rating` ADD CONSTRAINT `FK_e183e272ec7c5cd48cd09768848` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_rating` ADD CONSTRAINT `FK_85051608ff543aabf9c083b2590` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books` ADD CONSTRAINT `FK_bb8627d137a861e2d5dc8d1eb20` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_73284a017a1b03deabe9ed279e1` FOREIGN KEY (`banStatusId`) REFERENCES `ban_statuses`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_73284a017a1b03deabe9ed279e1`");
        await queryRunner.query("ALTER TABLE `books` DROP FOREIGN KEY `FK_bb8627d137a861e2d5dc8d1eb20`");
        await queryRunner.query("ALTER TABLE `books_rating` DROP FOREIGN KEY `FK_85051608ff543aabf9c083b2590`");
        await queryRunner.query("ALTER TABLE `books_rating` DROP FOREIGN KEY `FK_e183e272ec7c5cd48cd09768848`");
        await queryRunner.query("ALTER TABLE `books_reviews` DROP FOREIGN KEY `FK_d990fae43ca0533c6d5d4668cdf`");
        await queryRunner.query("ALTER TABLE `books_reviews` DROP FOREIGN KEY `FK_b5b1376760b41852a8ffd40a915`");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP FOREIGN KEY `FK_93405d7e2e6308c3ba6b42f6c48`");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP FOREIGN KEY `FK_4d550ca9301c46d2dababd34438`");
        await queryRunner.query("ALTER TABLE `reviews_votes` DROP FOREIGN KEY `FK_b225ec6b6a5811efc56aaef4b31`");
        await queryRunner.query("ALTER TABLE `ban_statuses` CHANGE `bannedOn` `bannedOn` datetime NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` CHANGE `banStatusId` `banStatusId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` CHANGE `registeredOn` `registeredOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_73284a017a1b03deabe9ed279e1` FOREIGN KEY (`banStatusId`) REFERENCES `ban_statuses`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books` CHANGE `userId` `userId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `books` ADD CONSTRAINT `FK_bb8627d137a861e2d5dc8d1eb20` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_rating` CHANGE `bookId` `bookId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `books_rating` CHANGE `userId` `userId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `books_rating` ADD CONSTRAINT `FK_85051608ff543aabf9c083b2590` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_rating` ADD CONSTRAINT `FK_e183e272ec7c5cd48cd09768848` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_reviews` CHANGE `bookId` `bookId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `books_reviews` CHANGE `authorId` `authorId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `books_reviews` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `books_reviews` ADD CONSTRAINT `FK_d990fae43ca0533c6d5d4668cdf` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_reviews` ADD CONSTRAINT `FK_b5b1376760b41852a8ffd40a915` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` CHANGE `reviewId` `reviewId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `reviews_flags` CHANGE `authorId` `authorId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD CONSTRAINT `FK_93405d7e2e6308c3ba6b42f6c48` FOREIGN KEY (`reviewId`) REFERENCES `books_reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD CONSTRAINT `FK_4d550ca9301c46d2dababd34438` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_votes` CHANGE `reviewId` `reviewId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `reviews_votes` ADD CONSTRAINT `FK_b225ec6b6a5811efc56aaef4b31` FOREIGN KEY (`reviewId`) REFERENCES `books_reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP COLUMN `createdOn`");
    }

}
