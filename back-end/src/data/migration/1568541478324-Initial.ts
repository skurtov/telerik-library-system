import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1568541478324 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `reviews_votes` (`id` varchar(36) NOT NULL, `like` tinyint NOT NULL DEFAULT 0, `username` varchar(255) NOT NULL, `reviewId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `reviews_flags` (`id` varchar(36) NOT NULL, `description` varchar(255) NOT NULL, `isChecked` tinyint NOT NULL DEFAULT 0, `authorId` varchar(36) NULL, `reviewId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books_reviews` (`id` varchar(36) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `content` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `authorId` varchar(36) NULL, `bookId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books_rating` (`id` varchar(36) NOT NULL, `rating` int NOT NULL, `userId` varchar(36) NULL, `bookId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `books` (`id` varchar(36) NOT NULL, `title` varchar(255) NOT NULL, `author` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `status` enum ('0', '1', '2') NOT NULL DEFAULT '2', `userId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `registeredOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `banStatusId` varchar(36) NULL, UNIQUE INDEX `IDX_fe0bb3f6520ee0469504521e71` (`username`), UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), UNIQUE INDEX `REL_73284a017a1b03deabe9ed279e` (`banStatusId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `ban_statuses` (`id` varchar(36) NOT NULL, `isBanned` tinyint NOT NULL DEFAULT 0, `description` varchar(255) NOT NULL, `bannedOn` datetime NULL DEFAULT null, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_borrowed_books_books` (`usersId` varchar(36) NOT NULL, `booksId` varchar(36) NOT NULL, INDEX `IDX_fa19704491e14f70b7cd133512` (`usersId`), INDEX `IDX_2f895ae53ab125599c152fa6ba` (`booksId`), PRIMARY KEY (`usersId`, `booksId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `reviews_votes` ADD CONSTRAINT `FK_b225ec6b6a5811efc56aaef4b31` FOREIGN KEY (`reviewId`) REFERENCES `books_reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD CONSTRAINT `FK_4d550ca9301c46d2dababd34438` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `reviews_flags` ADD CONSTRAINT `FK_93405d7e2e6308c3ba6b42f6c48` FOREIGN KEY (`reviewId`) REFERENCES `books_reviews`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_reviews` ADD CONSTRAINT `FK_b5b1376760b41852a8ffd40a915` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_reviews` ADD CONSTRAINT `FK_d990fae43ca0533c6d5d4668cdf` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_rating` ADD CONSTRAINT `FK_e183e272ec7c5cd48cd09768848` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books_rating` ADD CONSTRAINT `FK_85051608ff543aabf9c083b2590` FOREIGN KEY (`bookId`) REFERENCES `books`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `books` ADD CONSTRAINT `FK_bb8627d137a861e2d5dc8d1eb20` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_73284a017a1b03deabe9ed279e1` FOREIGN KEY (`banStatusId`) REFERENCES `ban_statuses`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_borrowed_books_books` ADD CONSTRAINT `FK_fa19704491e14f70b7cd133512d` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_borrowed_books_books` ADD CONSTRAINT `FK_2f895ae53ab125599c152fa6ba1` FOREIGN KEY (`booksId`) REFERENCES `books`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users_borrowed_books_books` DROP FOREIGN KEY `FK_2f895ae53ab125599c152fa6ba1`");
        await queryRunner.query("ALTER TABLE `users_borrowed_books_books` DROP FOREIGN KEY `FK_fa19704491e14f70b7cd133512d`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_73284a017a1b03deabe9ed279e1`");
        await queryRunner.query("ALTER TABLE `books` DROP FOREIGN KEY `FK_bb8627d137a861e2d5dc8d1eb20`");
        await queryRunner.query("ALTER TABLE `books_rating` DROP FOREIGN KEY `FK_85051608ff543aabf9c083b2590`");
        await queryRunner.query("ALTER TABLE `books_rating` DROP FOREIGN KEY `FK_e183e272ec7c5cd48cd09768848`");
        await queryRunner.query("ALTER TABLE `books_reviews` DROP FOREIGN KEY `FK_d990fae43ca0533c6d5d4668cdf`");
        await queryRunner.query("ALTER TABLE `books_reviews` DROP FOREIGN KEY `FK_b5b1376760b41852a8ffd40a915`");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP FOREIGN KEY `FK_93405d7e2e6308c3ba6b42f6c48`");
        await queryRunner.query("ALTER TABLE `reviews_flags` DROP FOREIGN KEY `FK_4d550ca9301c46d2dababd34438`");
        await queryRunner.query("ALTER TABLE `reviews_votes` DROP FOREIGN KEY `FK_b225ec6b6a5811efc56aaef4b31`");
        await queryRunner.query("DROP INDEX `IDX_2f895ae53ab125599c152fa6ba` ON `users_borrowed_books_books`");
        await queryRunner.query("DROP INDEX `IDX_fa19704491e14f70b7cd133512` ON `users_borrowed_books_books`");
        await queryRunner.query("DROP TABLE `users_borrowed_books_books`");
        await queryRunner.query("DROP TABLE `ban_statuses`");
        await queryRunner.query("DROP INDEX `REL_73284a017a1b03deabe9ed279e` ON `users`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP INDEX `IDX_fe0bb3f6520ee0469504521e71` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `books`");
        await queryRunner.query("DROP TABLE `books_rating`");
        await queryRunner.query("DROP TABLE `books_reviews`");
        await queryRunner.query("DROP TABLE `reviews_flags`");
        await queryRunner.query("DROP TABLE `reviews_votes`");
    }

}
