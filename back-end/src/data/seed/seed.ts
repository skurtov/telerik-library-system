import { createConnection } from 'typeorm';
import { User } from '../entities/user.entity';
import { BanStatus } from '../entities/ban-status.entity';
import { Book } from '../entities/book.entity';
import * as bcrypt from 'bcrypt';
import { BookReview } from '../entities/book-review.entity';

const main = async () => {
    const connection = await createConnection();

    const userRepo = connection.manager.getRepository(User);
    const banRepo = connection.manager.getRepository(BanStatus);
    const bookRepo = connection.manager.getRepository(Book);
    const reviewRepo = connection.manager.getRepository(BookReview);

    const banStatus1 = new BanStatus();
    banStatus1.description = 'not banned';
    banStatus1.isBanned = false;
    await banRepo.save(banStatus1);

    const banStatus2 = new BanStatus();
    banStatus2.description = 'not banned';
    banStatus2.isBanned = false;
    await banRepo.save(banStatus2);

    const user1 = new User();
    user1.username = 'kiro';
    const password = await bcrypt.hash('qwerty', 10);
    user1.password = password;
    user1.banStatus = Promise.resolve(banStatus1);
    user1.email = 'kiro@kiro.com';
    await userRepo.save(user1);

    const user2 = new User();
    user2.username = 'mitko';
    const password2 = await bcrypt.hash('123456', 10);
    user2.password = password2;
    user2.banStatus = Promise.resolve(banStatus2);
    user2.email = 'mitko@mitko.com';
    await userRepo.save(user2);

    const book1 = new Book();
    book1.author = 'Gorbachov';
    book1.title = 'Not great not terrible';
    book1.resume = 'First book resume';
    book1.oldUsers = Promise.resolve([user2]);
    book1.isbn = '123-123-123';
    await bookRepo.save(book1);

    const book2 = new Book();
    book2.author = 'Kiro skalata';
    book2.title = 'Asteroidi';
    book2.resume = 'second book resume';
    book2.isbn = '@@@-123-123';
    await bookRepo.save(book2);

    const book3 = new Book();
    book3.author = 'Misho skalata';
    book3.title = 'Kriptoni';
    book3.resume = 'Third book resume';
    book3.isbn = '@@@-123-!!!';
    await bookRepo.save(book3);

    const review1 = new BookReview();
    review1.author = Promise.resolve(user2);
    review1.book = Promise.resolve(book1);
    review1.content = 'Some content from Mitko!';
    await reviewRepo.save(review1);

    connection.close();
};

// tslint:disable-next-line:no-console
main().catch(console.error);
