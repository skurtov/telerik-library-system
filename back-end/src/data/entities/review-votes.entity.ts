import { Entity,  PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { BookReview } from './book-review.entity';

@Entity('reviews_votes')
export class ReviewVote {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({default: false})
    like: boolean;
    @Column()
    username: string;
    @ManyToOne(type => BookReview, review => review.votes)
    review: Promise<BookReview>;
}
