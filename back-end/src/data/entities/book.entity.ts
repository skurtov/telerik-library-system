import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, ManyToMany, JoinColumn, ManyToOne, JoinTable } from 'typeorm';
import { BookStatus } from '../../models/enums/book-status';
import { User } from './user.entity';
import { BookReview } from './book-review.entity';
import { BookRating } from './book-rating.entity';

@Entity('books')
export class Book {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    title: string;
    @Column()
    author: string;
    @Column({default: false})
    isDeleted: boolean;
    @Column()
    resume: string;
    @Column({
        type: 'enum',
        enum: BookStatus,
        default: BookStatus.FREE,
    })
    status: BookStatus;
    @Column()
    isbn: string;
    @ManyToOne(type => User, user => user.books)
    user: Promise<User>;
    @ManyToMany(type => User, user => user.borrowedBooks)
    oldUsers: Promise<User[]>;
    @OneToMany(type => BookReview, review => review.book)
    reviews: Promise<BookReview[]>;
    @OneToMany(type => BookRating, rate => rate.book)
    rates: Promise<BookRating[]>;
}
