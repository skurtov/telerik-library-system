import { Entity, PrimaryGeneratedColumn, CreateDateColumn, Column, ManyToOne, OneToMany, UpdateDateColumn } from 'typeorm';
import { User } from './user.entity';
import { Book } from './book.entity';
import { ReviewVote } from './review-votes.entity';
import { ReviewFlag } from './review-flag.entity';

@Entity('books_reviews')
export class BookReview {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @CreateDateColumn()
    createdOn: Date;
    @Column()
    content: string;
    @Column({default: false})
    isDeleted: boolean;
    @ManyToOne(type => User, user => user.bookReviews)
    author: Promise<User>;
    @ManyToOne(type => Book, book => book.reviews)
    book: Promise<Book>;
    @OneToMany(type => ReviewVote, like => like.review)
    votes: Promise<ReviewVote[]>;
    @OneToMany(type => ReviewFlag, flag => flag.review)
    flags: Promise<ReviewFlag[]>;
}
