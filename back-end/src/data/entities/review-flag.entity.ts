import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn } from 'typeorm';
import { User } from './user.entity';
import { BookReview } from './book-review.entity';

@Entity('reviews_flags')
export class ReviewFlag {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column()
    description: string;
    @Column('boolean', {default: false})
    isChecked: boolean;
    @CreateDateColumn()
    createdOn: Date;
    @ManyToOne(type => User, user => user.reviewFlags)
    author: Promise<User>;
    @ManyToOne(type => BookReview, review => review.flags)
    review: Promise<BookReview>;
}
