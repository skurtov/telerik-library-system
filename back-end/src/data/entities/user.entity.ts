import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToOne, OneToMany, JoinColumn, ManyToMany, JoinTable } from 'typeorm';
import { BanStatus } from './ban-status.entity';
import { Book } from './book.entity';
import { BookReview } from './book-review.entity';
import { BookRating } from './book-rating.entity';
import { ReviewFlag } from './review-flag.entity';
import { UserRoles } from '../../models/enums/user-roles';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({unique: true})
    username: string;
    @Column()
    password: string;
    @Column({unique: true})
    email: string;
    @CreateDateColumn()
    registeredOn: Date;
    @Column({default: false})
    isDeleted: boolean;
    @Column({
        type: 'enum',
        enum: UserRoles,
        default: UserRoles.USER,
    })
    rights: UserRoles;
    @JoinColumn()
    @OneToOne(type => BanStatus, banStatus => banStatus.user, { cascade: true})
    banStatus: Promise<BanStatus>;
    @OneToMany(type => Book, book => book.user)
    books: Promise<Book[]>;
    @JoinTable()
    @ManyToMany(type => Book, book => book.oldUsers)
    borrowedBooks: Promise<Book[]>;
    @OneToMany(type => BookReview, review => review.author)
    bookReviews: Promise<BookReview[]>;
    @OneToMany(type => BookRating, rate => rate.user)
    bookRates: Promise<BookRating[]>;
    @OneToMany(type => ReviewFlag, flag => flag.author)
    reviewFlags: Promise<ReviewFlag[]>;
}
