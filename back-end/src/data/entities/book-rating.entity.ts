import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';
import { Book } from './book.entity';
import { IsInt, Min, Max } from 'class-validator';

@Entity('books_rating')
export class BookRating {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @IsInt()
    @Min(0)
    @Max(10)
    @Column()
    rating: number;
    @ManyToOne(type => User, user => user.bookRates)
    user: Promise<User>;
    @ManyToOne(type => Book, book => book.rates)
    book: Promise<Book>;
}
