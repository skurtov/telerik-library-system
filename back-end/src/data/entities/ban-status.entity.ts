import { Entity,  PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('ban_statuses')
export class BanStatus {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    @Column({default: false})
    isBanned: boolean;
    @Column()
    description: string;
    @Column({default: null , nullable: true})
    bannedOn: Date;
    @OneToOne(type => User, user => user.banStatus)
    user: Promise<User>;
}
