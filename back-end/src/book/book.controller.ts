import { Controller, UseGuards, HttpStatus, HttpCode, Get, Param,
    Req, Body, Put,  ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BookService } from './book.service';
import { ShowBookDTO } from '../models/dto/book/show-book.dto';
import { BookRatingDTO } from '../models/dto/book/book-rating.dto';
import { BanGuard } from '../guards/ban.guard';

@Controller()
export class BookController {

    public constructor(
        private readonly bookService: BookService,
    ) {}

    @Get('/books')
    // @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async getAllBooks(): Promise<ShowBookDTO[]> {
        return await this.bookService.getAllBooks();
    }

    @Get('/books/:bookId')
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async getIndividualBook(@Param('bookId') bookId: string, @Req() request: any): Promise<ShowBookDTO> {
        return await this.bookService.getIndividualBook(bookId, request.user);
    }

    @Put('/books/:bookId/borrow')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async borrowBook(@Param('bookId') bookId: string, @Req() request: any): Promise<any> {
        return await this.bookService.borrowBook(bookId, request.user);
    }

    @Put('books/:bookId/return')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async returnBook(@Param('bookId') bookId: string, @Req() request: any): Promise<any> {
        return await this.bookService.returnBook(bookId, request.user);
    }

    @Put('books/:bookId/rating')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async rateBook(
        @Body(new ValidationPipe({whitelist: true, transform: true})) rating: BookRatingDTO,
        @Req() request: any,
        @Param('bookId') bookId: string,
        ) {
        return await this.bookService.rateBook(rating, request.user, bookId);
    }

}
