import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from '../data/entities/book.entity';
import { Repository, Not } from 'typeorm';
import { ShowBookDTO } from '../models/dto/book/show-book.dto';
import { User } from '../data/entities/user.entity';
import { BookRatingDTO } from '../models/dto/book/book-rating.dto';
import { BookRating } from '../data/entities/book-rating.entity';
import { AddBookDTO } from '../models/dto/book/add-book.dto';

@Injectable()
export class BookService {

    public constructor(
        @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
        @InjectRepository(BookRating) private readonly bookRatingRepository: Repository<BookRating>,
    ) {}

    public async getAllBooks(): Promise<ShowBookDTO[]> {
        const foundBooks = await this.bookRepository.find({
            where: {
                status: Not('1'),
                isDeleted: false,
            },
            relations: ['user'],
        });
        return foundBooks;
    }

    public async getBookById(id: string): Promise<any> {
        return await this.bookRepository.findOne({
            where: {
                id,
                isDeleted: false,
                status: Not('1'),
            },
            relations: ['user'],
        });
    }
    public async getIndividualBook(id: string, user: User): Promise<ShowBookDTO> {
        const foundBook = await this.getBookById(id);
        if (user.rights === 1 || foundBook.status !== 1) {
            foundBook.oldUsers = await foundBook.oldUsers;
            return foundBook;
        }
        throw new NotFoundException('The book is not found!');
    }

    public async borrowBook(bookId: string, user: User): Promise<any> {
        const foundBook = await this.getBookById(bookId);
        if (!foundBook) {
            throw new NotFoundException('The book is not found!');
        }

        if (foundBook.status === 0) {
            throw new BadRequestException('This book is already borrow!');
        }
        foundBook.user = Promise.resolve(user);
        foundBook.status = 0;
        return await this.bookRepository.save(foundBook);
    }

    public async returnBook(bookId: string, user: User): Promise<any> {
        const foundBook = await this.getBookById(bookId);
        const foundBookUser = await foundBook.user;
        if (!foundBookUser) {
            throw new BadRequestException('This book is not borrowed!');
        }
        if (!(foundBookUser.id === user.id)) {
          throw new BadRequestException('You didn\'t borrow this book!');
        }
        foundBook.user = null;
        foundBook.status = 2;
        const users = await foundBook.oldUsers;
        if (!users.find(oldUser => oldUser.id === user.id)) {
            users.push(user);
        }
        return await this.bookRepository.save(foundBook);
    }

    public async rateBook(rating: BookRatingDTO, user: User, bookId: string): Promise<any> {
        const foundBook = await this.getBookById(bookId);

        if (!foundBook) {
            throw new NotFoundException('The book is not found!');
        }
        const foundBookUsers = await foundBook.oldUsers;
        if (!foundBookUsers.find(oldUser => oldUser.id === user.id)) {
            throw new BadRequestException('To rate this book you must read it first!');
        }

        const rate = await this.bookRatingRepository.findOne({
            where: {
                book: bookId,
                user: user.id,
            },
        });

        if (rate) {
            return await this.bookRatingRepository.update({id: rate.id}, {rating: rating.rate});
        }
        const rateToSave = new BookRating();
        rateToSave.user = Promise.resolve(user);
        rateToSave.rating = rating.rate;
        rateToSave.book = Promise.resolve(foundBook);
        return await this.bookRatingRepository.save(rateToSave);
    }

    public async addBook(book: AddBookDTO): Promise<any> {

        const foundBook = await this.bookRepository.findOne({
            where: {
                title: book.title,
            },
        });
        if (foundBook) {
            throw new BadRequestException('This book has already been added!');
        }
        const bookToSave = new Book();
        bookToSave.author = book.author;
        bookToSave.isbn = book.isbn;
        bookToSave.title = book.title;
        return await this.bookRepository.save(bookToSave);
    }

    public async deleteBook(bookId: string): Promise<any> {
        const foundBook = await this.bookRepository.findOne({
            where: {
                id: bookId,
                isDeleted: false,
            },
        });
        if (!foundBook) {
            throw new NotFoundException('The book is not found!');
        }
        return await this.bookRepository.update({id: bookId}, {isDeleted: true});
    }

    public async unlistBook(bookId: string): Promise<any> {
        const foundBook = await this.getBookById(bookId);
        if (!foundBook) {
            throw new NotFoundException('The book is not found!');
        }

        if (await foundBook.user) {
            foundBook.user = null;
        }
        foundBook.status = 1;
        return await this.bookRepository.save(foundBook);
    }
}
