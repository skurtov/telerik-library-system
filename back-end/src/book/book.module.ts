import { Module } from '@nestjs/common';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from '../data/entities/book.entity';
import { BookRating } from '../data/entities/book-rating.entity';

@Module({
  imports: [
    CoreModule,
     AuthModule,
     TypeOrmModule.forFeature([Book, BookRating]),
    ],
  controllers: [BookController],
  providers: [BookService],
})
export class BookModule {}
