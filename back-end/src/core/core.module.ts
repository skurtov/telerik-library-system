import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { User } from '../data/entities/user.entity';
import { BookService } from '../book/book.service';
import { Book } from '../data/entities/book.entity';
import { BookRating } from '../data/entities/book-rating.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Book, BookRating]),
      ],
    providers: [UserService, BookService],
    exports: [UserService, TypeOrmModule, BookService],
})
export class CoreModule {}
