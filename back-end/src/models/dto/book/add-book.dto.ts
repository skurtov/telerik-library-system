import { IsString } from 'class-validator';

export class AddBookDTO {
    @IsString()
    title: string;
    @IsString()
    author: string;
    @IsString()
    isbn: string;
    @IsString()
    resume: string;
}
