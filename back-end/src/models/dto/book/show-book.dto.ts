import { BookStatus } from '../../enums/book-status';

export class ShowBookDTO {
    title: string;
    author: string;
    status: BookStatus;
}
