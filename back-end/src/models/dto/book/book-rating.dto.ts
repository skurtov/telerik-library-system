import { IsInt, Min, Max } from 'class-validator';

export class BookRatingDTO {
    @IsInt()
    @Min(0)
    @Max(10)
    rate: number;
}
