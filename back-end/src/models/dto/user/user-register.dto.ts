import { IsString, Length, IsEmail } from 'class-validator';

export class UserRegisterDTO {
    @IsString()
    @Length(3, 15)
    username: string;
    @IsEmail()
    email: string;
    @IsString()
    @Length(6, 20)
    password: string;
}
