import { IsString, MinLength } from 'class-validator';

export class BanUserDTO {
    @IsString()
    @MinLength(5)
    description: string;
}
