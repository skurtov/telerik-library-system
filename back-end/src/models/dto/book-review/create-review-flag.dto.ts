import { IsString, MinLength, MaxLength } from 'class-validator';

export class CreateReviewFlagDTO {
    @IsString()
    @MinLength(4)
    @MaxLength(200)
    description: string;
}
