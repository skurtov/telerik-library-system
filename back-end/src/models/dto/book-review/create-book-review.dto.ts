import { IsString, Length } from 'class-validator';

export class CreateBookReviewDTO {
    @IsString()
    @Length(3, 150)
    content: string;
}
