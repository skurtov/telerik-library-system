export enum BookStatus {
    BORROWED = 0,
    UNLISTED = 1,
    FREE = 2,
}
