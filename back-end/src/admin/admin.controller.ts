import { Controller, Post, HttpCode, HttpStatus, UseGuards, Body, ValidationPipe, Delete, Param, Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../guards/roles.guard';
import { AddBookDTO } from '../models/dto/book/add-book.dto';
import { BookService } from '../book/book.service';
import { UserService } from '../user/user.service';
import { BanUserDTO } from '../models/dto/user/ban-user.dto';

@Controller('admin')
export class AdminController {

    public constructor(
        private readonly bookService: BookService,
        private readonly userService: UserService,

    ) {}

    @Post('/books')
    @UseGuards(RolesGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.CREATED)
    public async addBook(@Body(new ValidationPipe({whitelist: true, transform: true})) book: AddBookDTO ): Promise<any> {
        return await this.bookService.addBook(book);
    }

    @Delete('/books/:bookId')
    @UseGuards(RolesGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async deleteBook(@Param('bookId') bookId: string): Promise<any> {
        return await this.bookService.deleteBook(bookId);
    }

    @Put('/books/:bookId')
    @UseGuards(RolesGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async unlistBook(@Param('bookId') bookId: string): Promise<any> {
        return await this.bookService.unlistBook(bookId);
    }

    @Put('/users/:userId/banstatus')
    @UseGuards(RolesGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async banUser(
        @Param('userId') userId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) banUser: BanUserDTO): Promise<any> {
        return await this.userService.banUser(userId, banUser);
    }

    @Delete('/users/:userId')
    @UseGuards(RolesGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async deleteUser(@Param('userId') userId: string): Promise<any> {
        return await this.userService.deleteUser(userId);
    }

}
