import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { BookService } from '../book/book.service';
import { Book } from '../data/entities/book.entity';
import { BookRating } from '../data/entities/book-rating.entity';

@Module({
  imports: [
    CoreModule,
    AuthModule,
    TypeOrmModule.forFeature([Book, BookRating]),
  ],
  controllers: [AdminController],
})
export class AdminModule {}
