import { Controller, Post, UseGuards, HttpCode, HttpStatus, Body, ValidationPipe, Param, Req, Get, Put, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateBookReviewDTO } from '../models/dto/book-review/create-book-review.dto';
import { BookReviewService } from './book-review.service';
import { CreateReviewFlagDTO } from '../models/dto/book-review/create-review-flag.dto';
import { BanGuard } from '../guards/ban.guard';

@Controller()
export class BookReviewController {

    public constructor(
        private readonly bookReviewService: BookReviewService,
    ) {}

    @Post('/books/:bookId/reviews')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.CREATED)
    public async createBookReview(@Body(new ValidationPipe({whitelist: true, transform: true}))
     review: CreateBookReviewDTO, @Param('bookId') bookId: string, @Req() request: any): Promise<any> {
        return await this.bookReviewService.createBookReview(review, bookId, request.user);
    }

    @Get('/books/:bookId/reviews')
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async getBookReviews(@Param('bookId') bookId: string): Promise<any> {
        return await this.bookReviewService.getBookReviews(bookId);
    }

    @Put('/books/:bookId/reviews/:reviewId')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async updateBookReview(
        @Param('reviewId') reviewId: string,
        @Body(new ValidationPipe({whitelist: true, transform: true})) review: CreateBookReviewDTO,
        @Req() request: any): Promise<any> {
            return await this.bookReviewService.updateBookReview(reviewId, request.user, review);
    }

    @Delete('/books/:bookId/reviews/:reviewId')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async deleteBookReview(@Param('reviewId') reviewId: string, @Req() request: any): Promise<any> {
        return await this.bookReviewService.deleteBookReview(reviewId, request.user);
    }

    @Put('reviews/:reviewId/votes')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async updateReviewVote(@Req() request: any, @Param('reviewId') reviewId: string ): Promise<any> {
        return await this.bookReviewService.updateReviewVote(request.user, reviewId);
    }

    @Post('/reviews/:reviewId/flags')
    @UseGuards(BanGuard)
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.CREATED)
    public async createReviewFlag(
        @Body(new ValidationPipe({transform: true, whitelist: true})) flag: CreateReviewFlagDTO,
        @Req() request: any,
        @Param('reviewId') reviewId: string,
    ): Promise<any> {
        return await this.bookReviewService.createReviewFlag(flag, request.user, reviewId);
    }
}
