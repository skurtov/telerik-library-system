import { Module } from '@nestjs/common';
import { BookReviewController } from './book-review.controller';
import { BookReviewService } from './book-review.service';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookReview } from '../data/entities/book-review.entity';
import { Book } from '../data/entities/book.entity';
import { ReviewVote } from '../data/entities/review-votes.entity';
import { ReviewFlag } from '../data/entities/review-flag.entity';

@Module({
  imports: [
    CoreModule,
     AuthModule,
     TypeOrmModule.forFeature([BookReview, Book, ReviewVote, ReviewFlag]),
    ],
  controllers: [BookReviewController],
  providers: [BookReviewService],
})
export class BookReviewModule {}
