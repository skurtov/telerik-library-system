import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookReview } from '../data/entities/book-review.entity';
import { Repository } from 'typeorm';
import { CreateBookReviewDTO } from '../models/dto/book-review/create-book-review.dto';
import { User } from '../data/entities/user.entity';
import { Book } from '../data/entities/book.entity';
import { ReviewVote } from '../data/entities/review-votes.entity';
import { CreateReviewFlagDTO } from '../models/dto/book-review/create-review-flag.dto';
import { ReviewFlag } from '../data/entities/review-flag.entity';

@Injectable()
export class BookReviewService {

    public constructor(
        @InjectRepository(BookReview) private readonly bookReviewRepo: Repository<BookReview>,
        @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
        @InjectRepository(ReviewVote) private readonly reviewVoteRepo: Repository<ReviewVote>,
        @InjectRepository(ReviewFlag) private readonly reviewFlagRepo: Repository<ReviewFlag>,
    ) {}

    public async createBookReview(review: CreateBookReviewDTO, bookId: string, user: User): Promise<any> {
        const foundBook = await this.bookRepository.findOne({
            where: {
                id: bookId,
                isDeleted: false,
            },
        });
        const oldUsers = await foundBook.oldUsers;

        if (!oldUsers.filter(oldUser => oldUser.id === user.id)) {
            throw new BadRequestException('You have to read the book to write a review!');
        }
        const reviewToSave = new BookReview();
        reviewToSave.author = Promise.resolve(user);
        reviewToSave.content = review.content;
        reviewToSave.book = Promise.resolve(foundBook);
        return await this.bookReviewRepo.save(reviewToSave);
    }

    public async getBookReviews(bookId: string): Promise<any> {
        return await this.bookReviewRepo.find({
            where: {
                book: bookId,
                isDeleted: false,
            },
            relations: ['author'],
        });
    }

    public async foundReviewById(reviewId: string): Promise<any> {
        return await this.bookReviewRepo.findOne({
            where: {
                id: reviewId,
                isDeleted: false,
            },
        });
    }

    public async updateBookReview(reviewId: string, user: User, reviewToUpdate: CreateBookReviewDTO): Promise<any> {
        const foundReview = await this.foundReviewById(reviewId);
        if (!foundReview) {
            throw new NotFoundException('The review is not found!');
        }
        if (user.rights) {
            foundReview.content = reviewToUpdate.content;
            return await this.bookReviewRepo.save(foundReview);
        }
        const foundReviewAuthor = await foundReview.author;
        if (foundReviewAuthor.id !== user.id) {
            throw new BadRequestException('You can update only yours reviews!');
        }
        foundReview.content = reviewToUpdate.content;
        return await this.bookReviewRepo.save(foundReview);

    }

    public async deleteBookReview(reviewId: string, user: User): Promise<any> {
        const foundReview = await this.foundReviewById(reviewId);
        if (!foundReview) {
            throw new NotFoundException('The review is not found!');
        }
        if (user.rights) {
            foundReview.isDeleted = true;
            return await this.bookReviewRepo.save(foundReview);
        }
        const foundReviewAuthor = await foundReview.author;
        if (foundReviewAuthor.id !== user.id) {
            throw new BadRequestException('You can delete only yours reviews!');
        }
        foundReview.isDeleted = true;
        return await this.bookReviewRepo.save(foundReview);
    }

    public async updateReviewVote(user: User, reviewId: string): Promise<any> {
        const foundReview = await this.foundReviewById(reviewId);
        if (!foundReview) {
            throw new NotFoundException('The review is not found!');
        }
        const foundReviewVotes = await foundReview.votes;
        if (foundReviewVotes.find(vote => vote.username === user.username)) {
            return await this.reviewVoteRepo.update({username: user.username}, {like: false});
        }
        const voteToSave = new ReviewVote();
        voteToSave.like = true;
        voteToSave.review = Promise.resolve(foundReview);
        voteToSave.username = user.username;
        return await this.reviewVoteRepo.save(voteToSave);
    }

    public async createReviewFlag(flag: CreateReviewFlagDTO, user: User, reviewId: string): Promise<any> {
        const foundReview = await this.foundReviewById(reviewId);
        if (!foundReview) {
            throw new NotFoundException('The review is not found!');
        }
        const flagToSave = new ReviewFlag();
        flagToSave.review = Promise.resolve(foundReview);
        flagToSave.author = Promise.resolve(user);
        flagToSave.description = flag.description;
        return await this.reviewFlagRepo.save(flagToSave);
    }
}
