import { CanActivate, Injectable, ExecutionContext, Inject } from '@nestjs/common';
import { User } from '../data/entities/user.entity';
import { REQUEST } from '@nestjs/core';

@Injectable()

export class BanGuard implements CanActivate {

    public constructor(
        @Inject(REQUEST) private readonly request: any,
    ) {}
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const user: User = this.request.user;
        const userStatus = await user.banStatus;
        return !userStatus.isBanned;
    }
}
