import { CanActivate, Injectable, ExecutionContext, Inject } from '@nestjs/common';
import { User } from '../data/entities/user.entity';
import { REQUEST } from '@nestjs/core';

@Injectable()

export class RolesGuard implements CanActivate {

    public constructor(
        @Inject(REQUEST) private readonly request: any,
    ) {}
    canActivate(context: ExecutionContext): boolean {
        const user: User = this.request.user;
        return !!user.rights;
    }
}
