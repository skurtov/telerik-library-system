import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config/config.service';
import { CoreModule } from './core/core.module';
import { AppService } from './app.service';
import { BookModule } from './book/book.module';
import { BookReviewModule } from './book-review/book-review.module';
import { AdminModule } from './admin/admin.module';

@Module({
  imports: [
    AuthModule,
     UserModule,
      ConfigModule,
      TypeOrmModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (configService: ConfigService) => ({
          type: configService.dbType as any,
          host: configService.dbHost,
          port: configService.dbPort,
          username: configService.dbUsername,
          password: configService.dbPassword,
          database: configService.dbName,
          entities: ['./src/data/entities/*.ts'],
        }),
      }),
      CoreModule,
      BookReviewModule,
      AdminModule,
      BookModule,
    ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
