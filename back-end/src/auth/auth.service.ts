import { Injectable } from '@nestjs/common';
import { UserRegisterDTO } from '../models/dto/user/user-register.dto';
import { UserService } from '../user/user.service';
import { ShowUserDTO } from '../models/dto/user/show-user.dto';
import { UserLoginDTO } from '../models/dto/user/user-login.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
    public constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
    ) {}
    public async validateIfUserExists(username: string): Promise<ShowUserDTO> {
        return await this.userService.findUserByUsername(username);
    }

    public async validateEmail(email: string): Promise<ShowUserDTO> {
        return await this.userService.findUserByEmail(email);
    }

    public async register(user: UserRegisterDTO): Promise<ShowUserDTO> {
        return await this.userService.addUser(user);
    }

    public async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
        return await this.userService.validateUserPassword(user);
    }

    public async login(user: UserLoginDTO): Promise<object> {
        const payload = { username: user.username };
        return { token: await this.jwtService.signAsync(payload)};
    }
}
