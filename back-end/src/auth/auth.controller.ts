import { Controller, Post, Body, ValidationPipe, BadRequestException,  UseGuards, Delete, HttpCode, HttpStatus} from '@nestjs/common';
import { UserRegisterDTO } from '../models/dto/user/user-register.dto';
import { AuthService } from './auth.service';
import { UserLoginDTO } from '../models/dto/user/user-login.dto';
import { AuthGuard } from '@nestjs/passport';
import { ShowUserDTO } from '../models/dto/user/show-user.dto';

@Controller('telerik-library')
export class AuthController {

    public constructor(
        private readonly authService: AuthService,
    ) {}

    @Post('/users')
    @HttpCode(HttpStatus.CREATED)
    public async registerUser(@Body(new ValidationPipe({whitelist: true, transform: true})) user: UserRegisterDTO): Promise<ShowUserDTO> {
        if (await this.authService.validateIfUserExists(user.username)) {
            throw new BadRequestException('User with such username already exists!');
          }
        if (await this.authService.validateEmail(user.email)) {
            throw new BadRequestException('User with such email already exists!');
        }
        return await this.authService.register(user);
    }

    @Post('/session')
    // @HttpCode(HttpStatus.CREATED)
    public async loginUser(@Body(new ValidationPipe({whitelist: true, transform: true})) user: UserLoginDTO): Promise<object> {
        if (!await this.authService.validateIfUserExists(user.username)) {
            throw new BadRequestException('User with this username does not exist!');
        }
        if (!await this.authService.validateUserPassword(user)) {
        throw new BadRequestException('Invalid password!');
        }
        return await this.authService.login(user);
    }

    @Delete('/session')
    @UseGuards(AuthGuard())
    @HttpCode(HttpStatus.OK)
    public async logoutUser(): Promise<object> {
        return { message: 'Successfully logged out!'} ;
    }

}
