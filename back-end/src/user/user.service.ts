import { Injectable, MethodNotAllowedException, NotFoundException } from '@nestjs/common';
import { UserRegisterDTO } from '../models/dto/user/user-register.dto';
import { ShowUserDTO } from '../models/dto/user/show-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from '../models/dto/user/user-login.dto';
import { BanStatus } from '../data/entities/ban-status.entity';
import { BanUserDTO } from '../models/dto/user/ban-user.dto';

@Injectable()
export class UserService {

    public constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
    ) {}

    public async findUserByUsername(username: string): Promise<any> {
        return await this.userRepository.findOne({
            where: {
                username,
            },
        });
    }

    public async findUserByEmail(email: string): Promise<ShowUserDTO> {
        const foundUser = await this.userRepository.findOne({
            where: {
                email,
            },
        });
        return foundUser;
    }

    public async addUser(user: UserRegisterDTO): Promise<ShowUserDTO> {
        user.username = user.username.toLowerCase();
        const password = await bcrypt.hash(user.password, 10);
        const userToAdd = new User();
        userToAdd.email = user.email;
        userToAdd.password = password;
        userToAdd.username = user.username;
        const banStatus = new BanStatus();
        banStatus.description = 'not banned';
        userToAdd.banStatus = Promise.resolve(banStatus);
        this.userRepository.save(userToAdd);
        return userToAdd;
    }

    public async validateUserPassword(user: UserLoginDTO): Promise<boolean> {
        const foundUser = await this.findUserByUsername(user.username);
        return await bcrypt.compare(user.password, foundUser.password);
    }

    public async banUser(userId: string, banUser: BanUserDTO): Promise<any> {
        const foundUser = await this.userRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
                rights: '0',
            },
        });

        if (!foundUser) {
            throw new NotFoundException('The user is not found!');
        }
        const foundUserStatus = await foundUser.banStatus;
        foundUserStatus.isBanned = true;
        foundUserStatus.description = banUser.description;
        foundUserStatus.bannedOn = new Date();
        return await this.userRepository.save(foundUser);
    }

    public async deleteUser(userId: string): Promise<any> {
        const foundUser = await this.userRepository.findOne({
            where: {
                id: userId,
            },
        });
        if (!foundUser) {
            throw new NotFoundException('The user not found!');
        }
        foundUser.isDeleted = true;
        return await this.userRepository.save(foundUser);
    }
}
