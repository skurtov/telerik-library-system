import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/core/services/book.service';
import { BookReviewService } from 'src/app/core/services/book-review.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StorageService } from 'src/app/core/services/storage.service';
import { ICreateReview } from 'src/app/models/create-review';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  public bookId: string;
  public book: any;
  public bookReviews: any[] = [];
  public btnText = 'Show reviews';
  public reviewForm: FormGroup;
  public loggedUser: string;
  public isBorrowBookBefore: boolean;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly bookService: BookService,
    private readonly bookReviewService: BookReviewService,
    private readonly notificator: NotificatorService,
    private readonly storageService: StorageService,
  ) { }

  public btnToggle() {
    if (this.btnText === 'Show reviews') {
    if (this.bookReviews.length === 0) {
      this.notificator.error('This book has no reviews!');
    }
    this.btnText = 'Hide reviews';
    } else {
      this.btnText = 'Show reviews';
    }
  }

  public createReview(formDirective) {
   const reviewToCreate: ICreateReview = this.reviewForm.value;
   this.bookReviewService.createReview(this.bookId, reviewToCreate)
   .subscribe((data) => {
    formDirective.resetForm();
    this.reviewForm.reset();
    this.btnText = 'Hide reviews';
    this.bookReviews.push(data);
    this.notificator.success('Successfully created review!');
   }, (error) => {
     console.log(error);
   });
  }

  public borrowBook(bookId: string) {
    this.bookService.borrowBook(bookId)
    .subscribe((data) => {
      this.book = data;
      this.notificator.success('Successfully borrowed book!');
    }, (error) => {
      console.log(error);
    });
  }

  public returnBook(bookId: string) {
    this.bookService.returnBook(bookId)
    .subscribe((data) => {
      this.book = data;
      this.notificator.success('Successfully returned book!');
      if (!this.isBorrowBookBefore) {
        this.isBorrowBookBefore = true;
      }
    }, (error) => {console.log(error); });
  }

  ngOnInit() {
    this.loggedUser = this.storageService.getItem('username');
        // tslint:disable-next-line: no-string-literal
    this.bookId = this.activatedRoute.snapshot.params['id'];
    this.book = this.bookService.individualBook(this.bookId)
    .subscribe((data) => {
      this.book = data;
      this.isBorrowBookBefore = !!this.book.__oldUsers__.find(oldUser => oldUser.username === this.loggedUser);
    }, (error) => {
      console.log(error);
    });

    this.reviewForm = new FormGroup({
      content: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(150),
      ]),
    });

    this.bookReviewService.getBookReviews(this.bookId)
      .subscribe((reviews) => {
        this.bookReviews = reviews;
      }, (error) => {
        console.log(error);
      });
  }

}
