import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllBooksViewComponent } from './all-books-view/all-books-view.component';
import { BookDetailsComponent } from './book-details/book-details.component';

const routes: Routes = [
  { path: '', component: AllBooksViewComponent, pathMatch: 'full' },
  { path: 'all-books', component: AllBooksViewComponent },
  { path: ':id', component: BookDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksRoutingModule {}