import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AllBooksViewComponent } from './all-books-view/all-books-view.component';
import { BooksRoutingModule } from './books-routing.module';
import { BookDetailsComponent } from './book-details/book-details.component';

@NgModule({
  declarations: [AllBooksViewComponent, BookDetailsComponent],
  imports: [
    SharedModule,
    BooksRoutingModule,
  ],
})
export class BooksModule { }
