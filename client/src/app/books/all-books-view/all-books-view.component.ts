import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService } from 'src/app/core/services/book.service';
import { StorageService } from 'src/app/core/services/storage.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-books-view',
  templateUrl: './all-books-view.component.html',
  styleUrls: ['./all-books-view.component.css']
})
export class AllBooksViewComponent implements OnInit {

  public books: any[];
  public loggedUser: string;
  public dataSource;
  public displayedColumns: string[] = ['position', 'title', 'author', 'ISBN', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly bookService: BookService,
    private readonly storageService: StorageService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }


  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public borrowBook(bookId: string) {
    this.bookService.borrowBook(bookId)
    .subscribe((data) => {
      this.books = this.books.map((book, index) => {
        if (book.id === bookId) {
          book = data;
        }
        book.position = index + 1;
        return book;
      });
      this.dataSource = this.books;
      this.notificator.success('Successfully borrowed book!');
    }, (error) => {
      console.log(error);
    });
  }

  public returnBook(bookId: string) {
    this.bookService.returnBook(bookId)
    .subscribe((data) => {
      this.books = this.books.map((book, index) => {
        if (book.id === bookId) {
          book = data;
        }
        book.position = index + 1;
        return book;
      });
      this.dataSource = this.books;
      this.notificator.success('Successfully returned book!');
    }, (error) => {console.log(error); });
  }

  public redirectToBookDetails(book: any): void {
    this.router.navigate(['/books/', book.id]);
  }

  ngOnInit() {
    this.loggedUser = this.storageService.getItem('username');
    this.bookService.allBooks()
    .subscribe((data) => {
      data = data.map((book, index) => {
        book.position = index + 1;
        return book;
      });
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.books = data;
    }, (error) => {
      console.log(error);
    });
  }

}
