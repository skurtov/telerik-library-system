import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/core/services/book.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public books;
  public displayedColumns: string[] = ['position', 'title', 'author', 'ISBN'];

  constructor(
    private readonly bookService: BookService,
  ) { }

  ngOnInit() {
    this.bookService.allBooks()
    .subscribe((data) => {
      data = data.map((book, index) => {
        book.position = index + 1;
        return book;
      });
      this.books = data;
      console.log(data);
    }, (error) => {
      console.log(error);
    });
  }

}
