import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Subscription } from 'rxjs';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  public isLoggedIn: boolean;
  public  isLoggedInSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  public logout(): void {
    this.authService.logout()
    .subscribe((data) => {
      this.notificator.success(data.message);
      this.router.navigate(['/home']);
    });
  }

  public ngOnInit() {
    this.isLoggedInSubscription = this.authService.isLoggedIn
    .subscribe(isLogged => this.isLoggedIn = isLogged);
  }

  public ngOnDestroy() {
    this.isLoggedInSubscription.unsubscribe();
  }

}
