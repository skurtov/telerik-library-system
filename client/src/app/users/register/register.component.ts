import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { IUserRegister } from 'src/app/models/user-register';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public hide = true;
  public registerForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  public getErrorMessage(formField) {
    if (formField === 'email') {
        return this.registerForm.controls.email.hasError('required') ? 'You must enter an email' :
            this.registerForm.controls.email.hasError('email') ? 'Not a valid email' :
              '';
    } else if (formField === 'username') {
      return this.registerForm.controls.username.hasError('required') ? 'You must enter a username' :
      this.registerForm.controls.username.hasError('minlength') ? 'Username must be greater than or equal to 3 characters' :
          '';
    } else if (formField === 'password') {
      return this.registerForm.controls.password.hasError('required') ? 'You must enter a password' :
      this.registerForm.controls.password.hasError('minlength') ? 'Password must be greater than or equal to 6 characters' :
        '';
    }
  }

  public register() {
    const user: IUserRegister = this.registerForm.value;
    this.authService.register(user)
    .subscribe(() => {
      this.router.navigate(['users/login']);
      this.notificator.success('Successfully registered!');
    }, response => {
      console.log(response);
      this.registerForm.controls.username.status === 'INVALID' ?
      this.notificator.error('Invalid username!') :
      this.registerForm.controls.email.status === 'INVALID' ?
      this.notificator.error('Invalid email!') :
      this.registerForm.controls.password.status === 'INVALID' ?
      this.notificator.error('Invalid password!') :
      this.notificator.error(response.error.message);
    });
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['',  [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

}
