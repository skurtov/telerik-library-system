import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUserLogin } from 'src/app/models/user-login';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public hide = true;
  public loginForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) { }

  public login() {
    const user: IUserLogin = this.loginForm.value;
    this.authService.login(user)
    .subscribe(() => {
      this.router.navigate(['/books/all-books']);
      this.notificator.success('Successfully logged in!');
    }, response => {
      this.loginForm.controls.username.status === 'INVALID' ?
      this.notificator.error('Invalid username!') :
      this.notificator.error(response.error.message);
    });
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['',  [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

}
