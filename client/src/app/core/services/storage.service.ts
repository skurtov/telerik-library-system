import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  public getItem(key: string): string {
    return localStorage.getItem(key);
  }

  public setItem(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  public removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  public clearStorage(): void {
    localStorage.clear();
  }
}
