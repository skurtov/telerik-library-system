import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICreateReview } from 'src/app/models/create-review';

@Injectable()
export class BookReviewService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getBookReviews(bookId: string): Observable<any> {
    return this.http.get(`http://localhost:3000/books/${bookId}/reviews`);
  }

  public createReview(bookId: string, reviewToCreate: ICreateReview): Observable<any> {
    return this.http.post(`http://localhost:3000/books/${bookId}/reviews`, reviewToCreate);
  }
}
