import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BookService {

  public constructor(
    private readonly http: HttpClient,
  ) { }

  public allBooks(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/books');
  }

  public borrowBook(bookId: string): Observable<any> {
    return this.http.put(`http://localhost:3000/books/${bookId}/borrow`, {});
  }

  public returnBook(bookId: string): Observable<any> {
    return this.http.put(`http://localhost:3000/books/${bookId}/return`, {});
  }

  public individualBook(bookId: string): Observable<any> {
    return this.http.get(`http://localhost:3000/books/${bookId}`);
  }
}
