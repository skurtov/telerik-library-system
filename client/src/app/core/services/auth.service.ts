import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { IUserRegister } from 'src/app/models/user-register';
import { IUserLogin } from 'src/app/models/user-login';
import { StorageService } from './storage.service';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService {

  private isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserAuthenticated());
  private helper = new JwtHelperService();

  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
  ) { }

  public register(user: IUserRegister): Observable<any> {
    return this.http.post<any>('http://localhost:3000/telerik-library/users', user);
  }

  public login(user: IUserLogin): Observable<any> {
    return this.http.post<any>('http://localhost:3000/telerik-library/session', user)
    .pipe(
      tap(
        data => {
        const decodedToken = this.helper.decodeToken(data.token);
        this.storageService.setItem('username', decodedToken.username);
        this.storageService.setItem('token', data.token);
        this.isLoggedInSubject$.next(true);
        }
      )
    );
  }

  public logout(): Observable<any> {
    return this.http.delete('http://localhost:3000/telerik-library/session')
    .pipe(
      tap(
        () => {
        this.storageService.removeItem('token');
        this.storageService.removeItem('username');
        this.isLoggedInSubject$.next(false);
        }
      )
    );
  }

  public get isLoggedIn(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  private isUserAuthenticated(): boolean {
    return !!this.storageService.getItem('token');
  }
}
