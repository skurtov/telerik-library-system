import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { NotificatorService } from './services/notificator.service';
import { BookService } from './services/book.service';
import { BookReviewService } from './services/book-review.service';

@NgModule({
  providers: [
    AuthService,
    StorageService,
    NotificatorService,
    BookService,
    BookReviewService
  ]
})
export class CoreModule {
  public constructor(
    @Optional() @SkipSelf() parent: CoreModule
  ) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
